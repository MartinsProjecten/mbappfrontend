//TYPES
import {
	ADD_ARTICLE, 
	IS_LOGGED_IN, 
	ADD_PROJECT, 
	ADD_PROJECTS,
	ADD_ALL_PROJECTS, 
	UPDATE_PROJECT,
	CLEAR_PROJECT
} from './types';

//ACTIONS
export const addArticle = article => ({type: ADD_ARTICLE, payload: article});
export const setIsLoggedIn = bool => ({type: IS_LOGGED_IN, payload: bool})

export const 
	addProject = project => ({type: ADD_PROJECT, payload: project}),
	updateProject = (payload, uid) => ({type: UPDATE_PROJECT, payload, uid}),
	addProjects = payload => ({type: ADD_PROJECTS, payload}),
	addAllProjects = payload => ({type: ADD_ALL_PROJECTS, payload}),
	clearProjects = () => ({type: CLEAR_PROJECT});
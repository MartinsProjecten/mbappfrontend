import React from 'react';
import {Info} from './info';
import {Box} from './box';

export const ErrorText = ({children}) => (
	<Info type={'error'}>
		{children}
	</Info>
);

export const ErrorBox = ({children}) => (
	<Box type={'error'}>{children}</Box>
);

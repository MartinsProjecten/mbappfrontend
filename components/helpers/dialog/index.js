import {ErrorText} from './error';
import {SuccessText} from './success';

export {ErrorText, SuccessText}
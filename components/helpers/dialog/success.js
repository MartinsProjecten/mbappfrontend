import React from 'react';
import {Info} from './info';

export const SuccessText = ({children}) => (
	<Info type={'success'}>
		{children}
	</Info>
)
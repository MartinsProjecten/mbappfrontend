import React from 'react';
import './index.scss';

export const Box = ({children, type}) => (
	<div className={'box box-'+type}>
		{children}
	</div>
);
import React from 'react';
import './index.scss';

export const Info = ({type, children}) => (
	<span className={'info info-'+type}>{children}</span>
);
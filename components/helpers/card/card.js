import React from 'react';
import './card.scss';

export const Card = ({children}) => (
	<article className="card">
    {children}
	</article>
);

export const CardHeader = ({title, children}) => (
  <header className="card-text-header">
    {children}
    
  </header> 
);

export const CardFooter = ({children}) => <footer className="card-text-footer">{children}</footer>;

export const CardContent = ({title, content, ...props}) => (
  <div className="card-text">
    <div className="card-text-content">
    <h2 className="card-text-title">{title}</h2>
      {content}
    </div>
  </div>
)
// export const CardImage = ({image}) => <div className="card-image"><img src={image} alt="afbeelding" /></div>;
export const CardImage = ({children}) => <div className="card-image">{children}</div>;
export const CardDate = ({created}) => <span className="card-text-published s-hide">{`Geplaats op ${created}`}</span>;

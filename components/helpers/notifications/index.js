const Notifications = {
	"NL": {
		NoArticles: "Er zijn nog geen artikelen geplaatst!",
		SomethingWentWrong: "Er is iets mis gegaan, probeer het later nog eens."

	},
	"EN": {
		NoArticles: `There are currently no articles yet!`,
		SomethingWentWrong: "Something went wrong, please try again later."
	}
}

// export const errors = {
// 	NoPostsYet: {
// 		"NL": `Er zijn nog geen ${posts} geplaatst!`,
// 		"EN": `There are currently no ${posts} yet!`,
// 	},
// }

export default Notifications;
import React from 'react';
import Field from './Field.js';
import Styles from './Styles.js';

export const RadioField = props => <Field {...props}> <Radio {...props} /></Field>;

// export const Radio = ({error, children,  ...rest }) => (
//   <div className="field-radio" style={error ? Styles.errorBorder : {}} {...rest} >
// 	 {children}
//   </div>
// )

export const Radio = ({error, options,  ...props }) => (
  <div className="field-radio" style={error ? Styles.errorBorder : {}} {...props} >
	 {options.map((option, key) => (
	 	<RadioOption key={key} titleName={option.title} value={option.value} {...props} />
	 ))}
  </div>
)

export const RadioOption = ({value, titleName, checked, ...props}) => (
  <label className='field-radio-label' htmlFor={value}>
    <input type="radio" id={value} value={value} checked={checked === value} {...props}/>
    <span>{titleName}</span>
  </label>
)


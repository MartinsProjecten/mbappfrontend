import React, {useState, useEffect} from 'react';
import Styles from './Styles.js';
import { Button } from './../button';

//const FileField = props => <Field {...props}> <File {...props} /></Field>;

function File(props) {
  const fileInput = React.createRef();
  const [fileName, setFileName] = useState('Geen bestand geselecteerd');
  const [fileSrc, setfileSrc] = useState('');

  useEffect(()=>{
    const reader  = new FileReader();
    const file = fileInput.current.files[0];
    reader.addEventListener("load", () => setfileSrc(reader.result), false);
    if (file) reader.readAsDataURL(file);
  },[fileName])

  const handleOnBlur = () => props.onFileChange(fileInput.current);

  const handleOnClick = event => {
    event.preventDefault();
    fileInput.current.click();
  }

  const onFileChange = () =>  { 
    if(fileInput.current.files.length > 0){
      setFileName(fileInput.current.files[0]['name'])
    }
  }

  return (
    <>
      <div className="field-file" style={props.error ? Styles.errorBorder : {}} onBlur= {handleOnBlur} >
        <Button onClick={handleOnClick}>Bladeren...</Button>
        <span style={{paddingLeft: '10px', display: 'inline-block', lineHeight: '44px'}}>{ fileName}</span>
      </div>
      <input
        style={{display: 'none'}}
        ref={fileInput}
        type='file'
        onChange = {onFileChange}
      />
      {fileSrc !== '' ? 
      <img src={fileSrc} width="100%" alt=" Afbeelding Voorbeeld..." /> 
      : props.children }
    </>
  )
}

export default File;

import React from 'react';
import Styles from './Styles.js';

const Field = props => {
  const {title = '', error = false, errortext='', children} = props;
  return (
    <label className="field">
      <span style={error ? Styles.errorColor : {}}>{title}</span>
      {children}
      <span style={Styles.errorColor}>{error ? errortext : ''}</span>
    </label>
  )
}

export default Field;

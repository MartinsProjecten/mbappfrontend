import React from 'react';
import Field from './Field.js';
import Styles from './Styles.js';

const TextField = props => <Field {...props}> <TextArea {...props} /></Field>;

const TextArea = ({rows = "6", error, id, ...rest}) => (
  <textarea
  	className="field-text"
  	name={id}
    {...rest}
    style={error ? Styles.errorBorder : {}}
    rows={rows}
  />
)
export default TextField;

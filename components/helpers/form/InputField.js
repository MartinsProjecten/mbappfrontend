import React from 'react';
import Field from './Field.js';
import Styles from './Styles.js';

const InputField = props => <Field {...props}> <Input {...props} /></Field>;

const Input = ({type='text', error,...rest }) => (
  <input
  className="field-input"
    {...rest}
    style={error ? Styles.errorBorder : {}}
    type={type}
  />
)

export default InputField;

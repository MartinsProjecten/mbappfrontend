const Styles = {
	errorColor: {
		color:  'hsl(0, 90%, 45%)',
		display: 'inline-block'
	},
  errorBorder: {
    borderColor: 'hsl(0, 90%, 45%)'
  }
}

export default Styles;

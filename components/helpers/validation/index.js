function Validate(value, name='De ingevoerde waarde') {
  return {
    error: [],
    isFile: true,
    required(customError = null) {
      if(value.length === 0){
          this.error = [...this.error, 
            typeof customError === 'string' ? 
            customError : 
            `${name} mag niet leeg zijn`
          ];
        }
      return this;
    },
    file(){
      if(value === '' || value === null || value === undefined) {
        this.error = [...this.error, `Je moet wel een bestand toevoegen`];
        this.isFile = false;
      }
      return this;
    },
    max(max = 255) {
      if(value.length > max){
        this.error = [...this.error, `${name} mag niet langer zijn dan ${max} karakters`];
      }
      return this;
    },
    min(min = 1) {
      if(value.length < min){
        this.error = [...this.error, `${name} moet minimaal ${min} karakters lang zijn`];
      }
      return this;
    },
    email(){
      const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       if(!value.match(re)){
          this.error = [...this.error, `${value} is niet een email`];
        }
      return this;      
    },
    compare(compareValue){
      if(value !== compareValue){
        this.error = [...this.error, `${name} is niet gelijk aan ${compareValue}`];
      }
      return this; 
    },
    size(bytes, type='kb') {
      if(this.isFile){
        let bs = bytes;
        switch(type){
          case 'mb':
            bs = bytes*1024*1024;
          break;
          case 'kb':
            bs = bytes*1024;
          break;
          default:
            bs = bytes
        }
        //const size = value.size / 1024;
        if(value.size > bs){
          this.error = [...this.error, `Het bestand ${(value.name).split('.')[0]} mag niet groter zijn dan ${bytes} ${type}`]
        }
      }
      return this; 
    },
    type() {
      if(this.isFile){
        const types = ['image/png', 'image/svg+xml', 'image/jpeg', 'image/gif'];
        if(!types.includes(value.type)){
           this.error = [...this.error, `Het bestand ${value.name} heeft niet het juiste type, het moet een png, jpg, svg of gif bestand zijn`]
        }
      }
      return this;
    },

    errors(){
      return this.error;
    }
  }

}

export default Validate;
import React from 'react';

export const ValidateErrors = (name, title, errors) => {
	let label = name;
	let color = ''
	if(errors[title]){
		if(errors[title].length !== 0) {
		 label = name + ' ' + errors[title].join(', ');
		 color = 'hsl(0, 90%, 45%)';
		}else {
			color = '';
			label = name;
		}

	}
	return (<span style={{color: color}}>{label}</span>)
}

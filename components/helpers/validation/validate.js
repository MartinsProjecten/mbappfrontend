  function validate(rules, name, value){
    let errors = [];
    for(const [rule, ruleValue] of Object.entries(rules)){
      // console.log(rule)
      //rule = (typeof rule === 'object') ? rule
    switch(rule){
      case 'required':
        if(value.length === 0){
          errors = [...errors, `mag niet leeg zijn`];
         
        }
      break;
      case 'max':
        if(value.length > ruleValue){
          errors = [...errors, `mag niet langer zijn dan ${ruleValue} karakters`];
        }
      break;
      case 'min':
        if(value.length < ruleValue){
          errors = [...errors, `moet langer zijn dan ${ruleValue} karakters`];
        }
      break;
      case 'email':
        if(value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)){
          errors = [...errors, `${value} is niet een email`];
        }
      break;
      default:
        // do nothing
      }
    }
  return errors;

  }

export default validate

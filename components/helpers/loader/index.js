import React from 'react';
import './loader.css';

const Loader = () => (<div className='flexloading'><span className='loader'><i className="fas fa-spinner fa-2x"></i></span></div>);


export default Loader;
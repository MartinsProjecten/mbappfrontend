export const localDateLong = (date, lang='nl-NL') => (
  date.toLocaleDateString(lang, { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })
)
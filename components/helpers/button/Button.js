import React from 'react';

export const Button = ({children, ...rest}) => (
	 <button type="button" className="button" {...rest} >{children}</button>
)
import React from 'react';
import { Link } from 'react-router-dom';

export const LinkButton = ({children, invert ='none' , ...rest}) => (
	 <Link className={`linked-button linked-button-${invert}`} {...rest}>{children}</Link>
)
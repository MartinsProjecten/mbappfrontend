function Logout(setIsAuth) {
	if(localStorage.getItem('AccessToken')){
		localStorage.removeItem('AccessToken');
		return setIsAuth(false);
	}
}

export default Logout;

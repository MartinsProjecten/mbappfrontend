
function Authenticate(isAuth, setIsAuth) {
  
  //get token
  //try if user is l
    const token = localStorage.getItem('AccessToken')
   
  
    if(!isAuth) {
      if(token){
        const apiUrl = process.env.REACT_APP_API_URL + '/api';
        fetch(`${apiUrl}/users/auth`, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          }
        })
        .then(response => {
          if(!response.ok) throw new Error('Er is iets mis gegaan');
          return response.json();
        })
        .then(json => {
          if(json.success){
            console.log('success auth')
           setIsAuth();
           console.log('from Authenticate', isAuth)
           return true;
          }
        })
        .catch(error => {
          console.log(error);
        })
      }else {
        return false;
      }
      
    }
    return true;
    
      
  
}


export default Authenticate;
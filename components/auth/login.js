import React, { useState } from 'react';
import { connect } from 'react-redux';
import Validator from './../helpers/validation';
import {InputField} from './../helpers/form';
import {ErrorText} from './../helpers/dialog';
import { Button } from './../helpers/button';


function Login({setIsAuth, history, setIsLoggedIn, isAuth}){
	const [values, setValues] = useState({email: '', password: ''});
  const [errors, setErrors] = useState({ email:'', password: ''});
  const [error, setError] = useState('');
  console.log(isAuth)

	const submitHandler = e => {
		e.preventDefault();

    setErrors({
      email: Validator(values.email, 'Email').required().email().max().errors().join(', '), 
      password: Validator(values.password, 'Wachtwoord').required().max().errors().join(', ')
    });

    if(Object.keys(errors).filter(e => errors[e]).length === 0){
      authToken(values);
      setValues({email: '', password: ''});
    }
    
  }
  
  const setLocalStorageToken = token => localStorage.setItem('AccessToken', token);

  const setAuth = token => {
    setLocalStorageToken(token);
    setIsAuth();
    setIsLoggedIn(true);
    history.push('/');
  }

  const authToken = loginCredentials => {
    const apiUrl = process.env.REACT_APP_API_URL + '/api';
    fetch(`${apiUrl}/users/auth`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(loginCredentials)
      })
    .then(response => {
      if(!response.ok){
        setError('E-mail en wachtwoord combinatie niet juist!')
      }
      return response.json();
    })
    .then(token => {
      if(!token.error) {
        return setAuth(token);
      }
      console.log('Error', token.error)
    })
    .catch(error => {
      console.log(error);
      setError('Er ging iets mis, probeer het later nog eens')
    });
  }

  const checkEmail = value => (
    Validator(value).required('Je moet wel een email adres invullen').email().max().errors()[0]
  )

  const checkPassword = value => (
    Validator(value, 'Wachtwoord').required().max().errors()[0]
  )

  const checkForErrors = (event, validation) => {
    const {name, value} = event.target;
    setErrors(prevState => (
      {...prevState, [name]: validation(value)}
    ))
  }

  const handleOnChange = e => {
    const {value, name} = e.target; 
    setValues(prevState => ({...prevState, [name]: value}))
  }

	return (
    <section>
      <h1>Inloggen</h1>
      <ErrorText>{error}</ErrorText>
      <form>
        <InputField 
          title='Email*' 
          name="email"
          errortext={errors.email} 
          error={errors.email ? true : false}
          onChange={handleOnChange} 
          value={values.email} 
          autoFocus
          onBlur={event => checkForErrors(event, checkEmail)}
          
        />
         <InputField 
          title='Wachtwoord*' 
          name="password"
          type="password"
          errortext={errors.password} 
          error={errors.password ? true : false}
          onChange={handleOnChange} 
          value={values.password} 
          onBlur={event => checkForErrors(event, checkPassword)}
        />
  			<Button type="submit" onClick={submitHandler} >Login</Button>
  		</form>
    </section>
  )
}


const mapStateToProps = state => ({
  isAuth: state.authReducer.isAuth,
});

const mapDispatchToProps = dispatch => ({
  setIsAuth: () => dispatch({type: 'IS_AUTHENTICATED'}),
  setIsLoggedIn: bool => dispatch({type: 'IS_LOGGED_IN', payload: bool})
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

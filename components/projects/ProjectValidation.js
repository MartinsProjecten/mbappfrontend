import Validate from './../helpers/validation';

 const ProjectValidation = {

    checkFile(value) {
       return Validate(value, 'Afbeelding').file().type().size(1, 'mb').errors()
    },
    checkTitle(value) {
      return Validate(value, 'Titel').required().max().errors()
    },
    checkContent(value) {
      return Validate(value, 'Content').required().min(20).errors()
    },
    checkRadio(value) {
      return Validate(value, 'Toegang').required('Kies één van de drie toegangsniveaus').errors()
    }
  }
export default ProjectValidation;
import React, {useState} from 'react';
import { connect} from 'react-redux';
import { addProject, updateProject } from '../../actions';
import ProjectCreate from './form/create';
import HocProject from './HocProject';
import './project.scss';

const mapDispatchToProps = {
  updateProject,
  addProject
}

const mapStateToProps = state => ({
  projects: state.projectReducer.projects
});

const EditProject = HocProject(ProjectCreate, 'aanpassen', true);
  
export default connect(mapStateToProps, mapDispatchToProps)(EditProject)

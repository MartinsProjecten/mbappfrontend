import React, {useState} from 'react';
import { connect} from 'react-redux';
import { addProject } from '../../actions';
import ProjectCreate from './form/create';
import HocProject from './HocProject';
import './project.scss';


const mapDispatchToProps = {
  addProject
}

const mapStateToProps = state => ({
  projects: state.projectReducer.projects
});

const CreateProject = HocProject(ProjectCreate, 'toevoegen');
  
export default connect(mapStateToProps, mapDispatchToProps)(CreateProject);

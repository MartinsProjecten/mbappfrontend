import React, {useEffect, useState} from 'react';
//import { Card, CardHeader, CardContent, CardImage, CardDate } from './../helpers/card';
import { connect} from 'react-redux';
import { Card } from './../helpers/card';
import { withRouter } from 'react-router-dom';
import { localDateLong } from './../helpers/date'


function Project({projects, history, projectData, children}){
  const apiUrl = process.env.REACT_APP_API_URL + '/api';
  const [project, setProject] = useState([]);

  useEffect(() => {
    if(project.length === 0) {
      setProject(prevState => ([...prevState, {title: 'project', image:'', content: '', created: ''}]));
      styleProject(projectData);
    }
  },[]);

  useEffect(() => {
    return styleProject(projectData);
  },[projects]);

  const styleProject = json => {
    const createdAt = json['created_at'] ? new Date(json['created_at']) : new Date(); 
    return  setProject({...json, created: localDateLong(createdAt), image: `${apiUrl}/files/${json['file_id']}`, children});
  }

  return (
    <Card {...project} />
  )
}


const mapStateToProps = state => ({
  projects: state.projectReducer.projects
});

export default withRouter(connect(mapStateToProps)(Project));
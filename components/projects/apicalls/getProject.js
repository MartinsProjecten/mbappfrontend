import Notification  from './../../helpers/notifications';

export default function GetProject(uid, setError, addProject, setValues){
  const apiUrl = process.env.REACT_APP_API_URL + '/api';
  return fetch(`${apiUrl}/projects/${uid}`)
    .then(response => {
      if(response.ok){
        return response.json();
      }
      return setError(Notification["NL"].SomethingWentWrong);
    })
    .then(json => {
      const {uid, title, content, file_id, accesslevel} = json;
      addProject({uid, title, content, file_id, accesslevel})
      if(typeof setValues === "function" && setValues !== undefined){
        setValues({title, content, file_id, accesslevel});
      } 
    })
    .catch(e => setError(Notification["NL"].SomethingWentWrong));
}
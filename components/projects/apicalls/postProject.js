import Notification  from './../../helpers/notifications';
const PostProject = ({formData, setError, ...props}) => {
  const apiUrl = process.env.REACT_APP_API_URL + '/api';
  
  fetch(apiUrl + '/projects', {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('AccessToken')}`
    },
    body: formData
   })
  .then(response => {
    if(response.ok){
      return response.json();
    } else {
      setError(Notification["NL"].SomethingWentWrong);
    }
    
  })
  .then(json => {
    if(json.error){
      console.log(json.error);
    } else {
      if(props.projects.length > 0){
        return props.addProject({...json});
      }
    }
  })
  .catch(error => {
    console.warn(error);
    setError(Notification["NL"].SomethingWentWrong);
  })
}

export default PostProject;
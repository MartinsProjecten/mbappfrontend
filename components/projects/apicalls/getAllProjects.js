import Notification  from './../../helpers/notifications';

const getAllProjects = (addAllProjects, setError, setLoading) => {
  setLoading(true);
  const apiUrl = process.env.REACT_APP_API_URL + '/api';
  //api class different
  const All = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('AccessToken')}`
    }};
	fetch(apiUrl + `/projects/all`,{
		...All
	})
	.then(response => {
		if(response.ok){
			return response.json()
		}
		return setError(Notification["NL"].SomethingWentWrong);
	})
	.then(json => {
		setLoading(false);
		return addAllProjects(json)
	})
	.catch(e =>{
      console.log(e);
      return setError(Notification["NL"].SomethingWentWrong);
    })
}

export default getAllProjects;
function CheckProjectTitle(value, setError, setValidationErrors){
	 const apiUrl = process.env.REACT_APP_API_URL + '/api';
	 return fetch(`${apiUrl}/projects/checktitle`, {
	 	method: 'POST',
	 	headers: {
	 		'Accept': 'application/json',
          	'Content-Type': 'application/json',
	 		//'Authorization': `Bearer ${localStorage.getItem('AccessToken')}`
	 	},
	 	body: JSON.stringify({title: value})
	 }).then(response => {
	 	if(response.ok){
	 		return response.json();
	 	}
	 	return setError('Er ging iets mis probeer het later nog eens');
	 }).then(json => {
	 	if(json.checktitle === true){
      setValidationErrors(prevState => ({...prevState, title: [`${value} bestaat al kies een andere titel`]}))
	 		return true;
	 	}
	 	return false;
	 })
}

export default CheckProjectTitle;


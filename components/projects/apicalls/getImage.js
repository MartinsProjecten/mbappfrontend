const getImage = (fileId, update, setLoading, setError) => {
	const apiUrl = process.env.REACT_APP_API_URL + '/api';
	setLoading(true);
	return fetch(`${apiUrl}/files/${fileId}`)
	.then(response => {
		if(response.ok){
			return response.blob()
		}
		return setError('Er ging iets mis')
	})
	.then(blob => {
		const objectUrl = URL.createObjectURL(blob);
    //var fileOfBlob = new File([blob], 'aFileName.json');
    setLoading(false);
    update(objectUrl)
    return //setImage(objectUrl)
	})
	// .then(stream => {
	// 	const reader = new FileReader();
	// 	reader.readAsDataURL(stream);
	// 	//const file = new File(reader)
	// 	reader.addEventListener("load", () => setImage(reader.result), false);
	// 	console.log('hallo', reader.readAsDataURL(stream).result)
	// })
	.catch(e => {
		console.log(e)
			return setError('Er ging iets mis')
		})
}

export default getImage;

// useEffect(()=>{
//     const reader  = new FileReader();
//     const file = fileInput.current.files[0];
//     reader.addEventListener("load", () => setfileSrc(reader.result), false);
//     if (file) reader.readAsDataURL(file);
//   },[fileName])
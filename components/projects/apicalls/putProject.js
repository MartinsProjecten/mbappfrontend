import Notification  from './../../helpers/notifications';
const PutProject = ({uid, formData, setError, ...props}) => {
  const apiUrl = process.env.REACT_APP_API_URL + '/api';
  
  fetch(apiUrl +  `/projects/cbb6fc794e7c9`, {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('AccessToken')}`
    },
    body: formData
   })
  .then(response => {
    if(response.ok){
      console.log(response.json())
      return response.json();
    } else {
      setError(Notification["NL"].SomethingWentWrong);
    }
    
  })
  .then(json => {
    if(json.error){
      console.log(json.error);
    } else {
      if(props.projects.length > 0){
        return props.updateProject(uid, json);
      }
    }
  })
  .catch(error => {
    console.warn(error);
    setError(Notification["NL"].SomethingWentWrong);
  })
}

export default PutProject;

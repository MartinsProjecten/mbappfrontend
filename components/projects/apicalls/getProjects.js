import Notification  from './../../helpers/notifications';

const getProjects = (addAllProjects, setError, setLoading) => {
	setLoading(true);
	const apiUrl = process.env.REACT_APP_API_URL + '/api';
	//api class different
	fetch(apiUrl + `/projects`)
	.then(response => {
		if(response.ok){
			return response.json()
		}
		return setError(Notification["NL"].SomethingWentWrong);
	})
	.then(json => {
		setLoading(false);
		return addAllProjects(json);
	})
	.catch(e =>{
	  console.log(e);
	  return setError(Notification["NL"].SomethingWentWrong);
	})
}

export default getProjects;
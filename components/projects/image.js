import React, {useState, useEffect} from 'react';
import getImage from './apicalls/getImage';
import Loader  from './../helpers/loader';
import { ErrorText } from './../helpers/dialog';
import {connect} from 'react-redux';
import { updateProject } from '../../actions';

import './project.scss';

function Image({updateProject, projects, title, fileId, ...rest}){
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState('');
  const project = projects.filter(project => project.file_id === fileId)[0];

  useEffect(() => {
    if(project.image === undefined){
      const update = image => updateProject({image}, project.uid)
      getImage(fileId, update, setLoading, setError);
    }
  }, []);

  const Figure = ({children}) => (
    <figure className='project-image' {...rest}>
      {children}
    </figure>
  );

  if(error) return (<Figure> <ErrorText>{error} </ErrorText> </Figure>)
  if(loading) return (<Figure>{Loader()}</Figure>)

	return (
		<Figure>
      <img src={project.image} alt={title} />
		</Figure>
	)

}

const mapStateToProps = state => ({
  projects: state.projectReducer.projects,
});

const mapDispatchToProps = {
   updateProject
}

export default connect(mapStateToProps, mapDispatchToProps)(Image);

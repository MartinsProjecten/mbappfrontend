import React, {useState, useEffect} from 'react';
import { connect} from 'react-redux';
import {InputField, Field, File, TextField, RadioField, RadioOption} from './../../helpers/form';
import { Button } from './../../helpers/button';
import GetProject from './../apicalls/getProject';
import CheckProjectTitle from './../apicalls/checkProjectTitle';
import postProject from './../apicalls/postProject';
import putProject from './../apicalls/putProject';
import validator from './../ProjectValidation';
import Image from './../image';

function ProjectCreate(props) {

	const [values, setValues] = useState({title: '', content: '', file: '', accesslevel: ''});
  const [error, setError] = useState('');
  const [validationErrors, setValidationErrors] = useState({
    title: [], 
    content: [], 
    file: [], 
    access: []
  });

	const { handleOnChange, handleForError, noErrors, submitHandler, isData=null, ButtonName, addProject} = props;

   useEffect(()=> {
    if(isData !== null){
      const { uid } = props.location.state;
      GetProject(uid, setError, addProject, setValues);
    }
  }, [])

  const checkForAllErrors = values => {
    setValidationErrors({
      title:  checkTitle(values.title), 
      content: validator.checkContent(values.content),
      file: validator.checkFile(values.file),
      access: validator.checkRadio(values.accesslevel),
    });
  }

  // const checkForAllErrors2 = values => {
  //   setValidationErrors({
  //     title:  checkTitle(values.title), 
  //     content: validator.checkContent(values.content),
  //     file: values.file === '' ? validator.checkFile(values.file): undefined ,
  //     access: validator.checkRadio(values.accesslevel),
  //   });
  // }


  const add = (formData, {projects, addProject}) => postProject({formData, setError, projects, addProject});

  const change = (formData, {projects, updateProject}) => {  
    const { uid } = props.location.state;
    console.log('is changing');
    return putProject({
      uid, 
      formData, 
      setError, 
      projects, 
      updateProject
    });
  }

  const action = setFormData => {  
    checkForAllErrors(values)
    if(props.noErrors(validationErrors)){
      const formData = setFormData(values)
      const execute = isData ? change : add;
      execute(formData, props);
      return props.history.push('/projecten');
    }  
  }

  const onFileChange = value => {
    const file = value.files[0] ? value.files[0] : value;
    setValues(prevState => ({...prevState, file}))
    handleForError({name: 'file', value: file}, validator.checkFile);
  } 

  const checkTitle = title => {
    if(title !== ''){
      CheckProjectTitle(title, setError, setValidationErrors);
    }
    return validator.checkTitle(title)
  }

  const onChanging = event => handleOnChange(event, setValues);

  const onValError = (event, validation) => handleForError(event.target, setValidationErrors, validation);
	
  return (
    <section>
      {isData ? <h1>Pas het project {values.title} aan</h1> : <h1>{props.isData}Voeg een project toe aan de project pagina</h1> }
      <form className="project-form">

        <InputField 
          title="Titel" 
          name="title" 
          error={validationErrors.title.length > 0 ? true : false}
          errortext={validationErrors.title[0]}
          onChange={onChanging}
          onBlur={event => onValError(event, checkTitle)}
          value={values.title}
          autoFocus
        />

        <Field title='Afbeelding' error={validationErrors.file.length > 0 ? true : false} errortext={validationErrors.file[0]} >
          <File onFileChange={onFileChange} error={validationErrors.file.length > 0 ? true : false}>
           {values.file_id ?  <Image title={values.title} fileId={values.file_id} {...props} />: null }
          </File>
        </Field>

        <TextField 
          title="Content" 
          name="content" 
          error={validationErrors.content.length > 0 ? true : false}
          errortext={validationErrors.content[0]}
          onChange={event => {
            onChanging(event)
            onValError(event, validator.checkContent);
          }}
           onBlur={event => {
            onValError(event, validator.checkContent);
          }}
          value={values.content}
        />

        <RadioField 
          title="Toegang" 
          name="accesslevel"
          error={validationErrors.access.length > 0 ? true : false}  
          errortext={validationErrors.access[0]}
          onBlur={event => onValError(event, validator.checkRadio)} 
          onChange={onChanging} 
          checked={values.accesslevel}
          options={[
            {value:'private', title:'Alleen ik'},
            {value: 'protected', title: 'Ingelogde Gebruikers'},
            {value: 'public', title: 'Iedereen'}
          ]}
        />

        <Button type="submit" onClick={event => submitHandler(event, action)} >{ButtonName}</Button>

      </form>
    
    </section>
  )
};

export default ProjectCreate

import React, {useState, useEffect} from 'react';
import { withRouter } from 'react-router-dom';
import { connect} from 'react-redux';
import { addProjects, clearProjects } from '../../actions';
import Project from './project';
import Image from './image';
import { LinkButton } from './../helpers/button';
import { Link } from 'react-router-dom';
import getProjects from './apicalls/getProjects';
import { ErrorText } from './../helpers/dialog';
import Loader  from './../helpers/loader';
import { Card, CardHeader, CardContent, CardImage, CardDate, CardFooter } from './../helpers/card';

import './project.scss';

function Projects({projects, addProjects, projectIsAuth, clearProjects, url, history}) {

	const [error, setError] = useState('');
	const [loading, setLoading] = useState(false);
  const [showId, setShowId] = useState('');



  useEffect(() => {
    console.log('projectslengt', projects, projectIsAuth)
    if(projectIsAuth) {
      console.log('vals')
      clearProjects();
    }
  }, [])

  useEffect(() => {
    console.log('projectslengt2', projects, projectIsAuth)
    if(projects.length === 0){
      getProjects(addProjects, setError, setLoading);
      //getImage('0fc954bb1ad82', setImage);
    }
  }, []);

   useEffect(() => {
    console.log('projectslengt3', projects, projectIsAuth)
    if(projects.length === 0){
      getProjects(addProjects, setError, setLoading);
    }
  }, [projectIsAuth]);

  useEffect( () => () => console.log("unmountProjects"), [] );

  

	if(error) return <ErrorText>{error}</ErrorText>;
	if(loading) return Loader();


	return (
		<section className='projects'>
      <article className='project'>
  			{projects.map((project, index) => 
          <Card key={index}>
            <CardHeader>
              <CardImage>
                <Image 
                  className="project-image" 
                  title={project.title} 
                  fileId={project.file_id} 
                />
              </CardImage>
            </CardHeader>
            <CardContent  {...project} />
            <CardFooter>
              <LinkButton to={`projects/${project.title}`} invert="invert">Bekijk meer</LinkButton>
              <LinkButton to={project.title} invert="invert">Ga naar project</LinkButton>
            </CardFooter>
          </Card>
        )}
      </article>
		</section>
	)
}

const mapStateToProps = state => ({
  projects: state.projectReducer.projects,
  projectIsAuth: state.projectReducer.isAuth,
});

const mapDispatchToProps = {
  addProjects,
  clearProjects
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Projects));

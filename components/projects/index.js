import Projects from './projects';
import React from 'react';
import getProjects from './apicalls/getProjects';

const ProjectsWithHoc = Projects;

export default ProjectsWithHoc
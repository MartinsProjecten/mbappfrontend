import React from 'react';

function HocProject(WrappedComponent, ButtonName, data) {
  return props => {
    //prime
    const submitHandler = (e, action) => {
      e.preventDefault();
      return action(setFormData);
    }

    //prime
    const noErrors = validationErrors => (
      Object.keys(validationErrors)
        .filter(f => validationErrors[f].length !== 0)
        .length ? false : true
    )

    //prime
    const handleForError = (event, setValidationErrors, validation) => {
      const {name, value} = event;
      setValidationErrors(prevState => ({...prevState, [name]: validation(value)}))
    }

    //prima
    const handleOnChange = (event, setValues) => {
      const {name, value} = event.target;
      setValues(prevState => ({...prevState, [name]: value}));
    }

    //prime
    const setFormData = values => {
      const formData = new FormData();
      Object.keys(values)
        .forEach(v => {
          formData.append(v, values[v])
        })
      return formData;
    }

    return (
      <WrappedComponent 
        handleOnChange = {handleOnChange}
        noErrors = {noErrors}
        handleForError = {handleForError}
        submitHandler= {submitHandler}
        ButtonName={ButtonName}
        isData={data}
        {...props}
      />
    )
  }
}

export default HocProject;

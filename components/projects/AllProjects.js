import React, {useState, useEffect} from 'react';
import { withRouter } from 'react-router-dom';
import { connect} from 'react-redux';
import { addProjects, clearProjects } from '../../actions';
import Project from './project';
import Image from './image';
import { LinkButton } from './../helpers/button';
import { Link } from 'react-router-dom';
import getAllProjects from './apicalls/getAllProjects';
import { ErrorText } from './../helpers/dialog';
import Loader  from './../helpers/loader';
import { Card, CardHeader, CardContent, CardImage, CardDate, CardFooter } from './../helpers/card';

import './project.scss';

const InfoWhenAuth = ({uid, ...props}) => {
  if(props.isAuth){
    return <Link className="article-button article-button-orange" 
      to={{ pathname: `/projecten/${uid}/aanpassen` ,state: {uid}}}>
    Aanpassen
    </Link> 
  }  
  return null;
}  

const AddProjectButton = () => <LinkButton to='projecten/toevoegen'> Nieuw project toevoegen </LinkButton>;

function AllProjects({projects, addProjects, projectIsAuth, clearProjects, isAuth, url, history}) {

	const [error, setError] = useState('');
	const [loading, setLoading] = useState(false);
  const [showId, setShowId] = useState('');

  

  useEffect(() => {
    if(!projectIsAuth) {
      clearProjects();
    }
  }, [])

  useEffect(() => {
    if(projects.length === 0){
      getAllProjects(addProjects, setError, setLoading);
    }
  }, [projectIsAuth]);

  useEffect(() => {
    if(projects.length === 0){
      getAllProjects(addProjects, setError, setLoading);
      //getImage('0fc954bb1ad82', setImage);
    }
  }, []);

   

  useEffect( () => () => console.log("unmountAllProjects"), [] );

//   useEffect( () => console.log("mount"), [] );
// useEffect( () => console.log("will update data1"), [ data1 ] );
// useEffect( () => console.log("will update any") );
// useEffect( () => () => console.log("will update data1 or unmount"), [ data1 ] );
// useEffect( () => () => console.log("unmount"), [] );

	if(error) return <ErrorText>{error}</ErrorText>;
	if(loading) return Loader();

	return (
		<section className='projects'>
     {isAuth ? <AddProjectButton isAuth={isAuth}/> : null}
      <article className='project'>
  			{projects.map((project, index) => 
          <Card key={index}>
            <CardHeader>
              <CardImage>
                <Image 
                  onClick={() => setShowId(project.file_id)} 
                  key={index} 
                  className="project-image" 
                  title={project.title} 
                  fileId={project.file_id} 
                />
              </CardImage>
            </CardHeader>
            <CardContent  {...project} />
            <CardFooter>
              <LinkButton to={`projects/${project.title}`} invert="invert">Bekijk meer</LinkButton>
              <LinkButton to={project.title} invert="invert">Ga naar project</LinkButton>
            </CardFooter>
            <InfoWhenAuth {...project} isAuth={isAuth} />
          </Card>
        )}
      </article>
		</section>
	)
}

const mapStateToProps = state => ({
  projects: state.projectReducer.projects,
  projectIsAuth: state.projectReducer.isAuth,
  isAuth: state.authReducer.isAuth
});

const mapDispatchToProps = {
  addProjects,
  clearProjects
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AllProjects));

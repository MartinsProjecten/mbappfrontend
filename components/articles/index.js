import React from 'react';
import Article from './article';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { addArticle, setIsLoggedIn } from './../../actions';
import loader from '../helpers/loader/index';
import {SuccessText} from '../helpers/dialog';

class Articles extends React.Component {

 // https://stackoverflow.com/questions/9439725/javascript-how-to-detect-if-browser-window-is-scrolled-to-bottom

  constructor(props){
    super(props);
    this.state= {
      error: '',
      fetched: false
    }
  }

  componentDidMount(){
    // if(!this.props.articlesFetchedFromApi) {
    //   this.props.articlesFetched();
    //   return this.fetchAllArticles();
    // } 
    console.log(this.props.articles);
    if(this.props.articles.length === 0){
      return this.fetchAllArticles();
    }
    this.setState({fetched: !this.state.fetched})
  }

  async fetchAllArticles() {  
    const apiUrl = process.env.REACT_APP_API_URL + '/api';
    try {
      const response = await fetch(`${apiUrl}/articles`);
      if(response.ok){
        const json = await response.json();
        this.setState({fetched: true})
        json.map(article => this.props.addArticle(
          {...article, url: `/artikelen/${article.title.replace(/ /g, '-')}`}
        ));
      }
      
    }catch(e){
      console.log(e);
      this.setState({error: 'Er ging iets mis, probeer het later nog een keer.'})
    }
   
  };

  render(){
    const {articles, isloggedIn, isAuth} = this.props;
    const {error, fetched} = this.state;

    if(articles.length === 0 && fetched === false){
      if(error !== '') return <p style={{color: 'red'}}>{error}</p>;
      return loader();
    }   
    
    return (
      <>
       <SuccessText>{isloggedIn?  'Je bent nu ingelogd' : '' }</SuccessText>
        {isAuth? <Link className="article-button" to='artikelen/toevoegen'> Nieuw artikel toevoegen </Link> : ''}
        {(articles.length === 0 && fetched) ? <p>Er zijn nog geen artikelen geschreven!</p> : ''}
        {articles.map((article,index) => {
          const a = {...article, isAuth}
          return (<Article key={index} {...a}>
            <Link className="article-button" to={{ pathname: article.url ,state: { id: article.id}}}>
            lees meer
            </Link>
          </Article>)
        }
          
        )} 
      </>
    )
  
  }
  
}

const mapDispatchToProps = {
  addArticle,
  setIsLoggedIn
};

const mapStateToProps = state => ({
  articles: state.articleReducer.articles,
  articlesFetchedFromApi: state.articleReducer.articlesFetchedFromApi,
  isloggedIn: state.authReducer.isloggedIn,
  isAuth: state.authReducer.isAuth,
});

export default connect(mapStateToProps, mapDispatchToProps)(Articles);

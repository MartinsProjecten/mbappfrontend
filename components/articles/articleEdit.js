import React from 'react';
import { connect } from 'react-redux';
import ArticleForm from './form/articleForm';

class EditArticle extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      values: {hoi: 'hallo'}
    }
    this.edit = this.edit.bind(this);
  }

  componentDidMount(){
    const requestArticle = async () => {
      const { id } = this.props.location.state;
      console.log(id);
      const article = this.props.articles.find(article =>  article.id === id);
      if(article){ 
        const {title, intro, content, id} = article;
        return this.setState({values: {title, intro, content, id}})
      }  
      const response = await fetch(`http://localhost/mbapp/public_html/api/articles/${id}`);
      if(response.ok){
        const {title, intro, content} = await response.json();
        return this.setState({values: {title, intro, content}})
      }
      return this.props.history.push('/artikelen');
    }
    requestArticle();
  }
   

 async edit(changedData) {
    try {
      const { id } = this.state.values;
      const apiUrl = process.env.REACT_APP_API_URL + '/api';
      const response = await fetch(`${apiUrl}/articles/${id}`, {
        method: 'PUT',
         headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(changedData)
      });
      const result = await response.json();
      if(!response.ok){
        if(response.status === 400){
          if(response.status > 400){
            //throw new Error(result);
            return this.props.history.push('/artikelen');
          }
          console.log(result);
          throw new Error({...result}); // validation errors 
        }
        return this.props.history.push('/artikelen');
      }
      this.props.updateArticle({...result, url: `/artikelen/${result.title.replace(' ', '-')}`});
      return this.props.history.push('/artikelen');
    }catch(error){
      console.log(error);
    }
  }

	render(){

    const { values } = this.state;

		return (
      <>
        <h1>Pas artikel {values.title} aan!</h1>
        <ArticleForm 
          submitName="AANPASSEN" 
          handleSubmitArticle={this.edit} 
          editValues={values}
          validate={{
            title: {required: 'required', min: 5}, 
            intro: {required: 'required', min: 5},
            content: {required: 'required', min: 5}
          }} 
        />
      </>
		)
	}

}

const mapStateToProps = state => ({
  articles: state.articleReducer.articles,
});

const mapDispatchToProps = dispatch => ({
  updateArticle: article => dispatch({type: 'UPDATE_ARTICLE', payload: article, id: article.id})
});

export default connect(mapStateToProps, mapDispatchToProps)(EditArticle);
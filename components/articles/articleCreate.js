import React from 'react';
import { connect } from 'react-redux';
import ArticleForm from './form/articleForm';

class CreateArticle extends React.Component {

  constructor(props){
    super(props);
    this.createArticle = this.createArticle.bind(this);

  }

  async createArticle(post) {
   const apiUrl = process.env.REACT_APP_API_URL + '/api';
   console.log(post);
    try {
      const response = await fetch(apiUrl + '/articles', {
        method: 'POST',
         headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
         // 'Authorization': `Bearer ${this.props.token}`
        },
        body: JSON.stringify(post)
      });
      const result = await response.json();
      if(!response.ok){
        if(response.status === 400){
          if(response.status > 400){
            //throw new Error(result);
            return this.props.history.push('/artikelen');
          }
          throw new Error(result); // validation errors 
        }
       
      }
      this.props.addArticle({...result, url: `/artikelen/${result.title.replace(' ', '-')}`});
      return this.props.history.push('/artikelen');
    }catch(error){
      console.log('krijg ik deze error voor niet ', error);
    }
  }

  render(){
    const funWords = ['fantastisch', 'geweldig', 'leuk', 'mooi'];
    const fun = Math.floor(Math.random() * funWords.length);
    return (
      <><h1>Schrijf een {funWords[fun]} artikel!!</h1>
        <ArticleForm submitName="TOEVOEGEN" handleSubmitArticle={this.createArticle} 
          validate={{
            title: {required: 'required', min: 5, max: 255}, 
            content: {required: 'required', min: 10}
          }} 
        />
      </>
    );
  }

}


const mapDispatchToProps = dispatch => ({
  addArticle: article => dispatch({type: 'ADD_ARTICLE', payload: article})
});

export default connect(null, mapDispatchToProps)(CreateArticle);
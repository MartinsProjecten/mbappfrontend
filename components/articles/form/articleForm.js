import React from 'react';
import './articleform.css'
import validate from './../../helpers/validation/validate';
import {InputField, TextField} from './../../helpers/form';
import { Button } from './../../helpers/button';


class artcileForm extends React.Component {

  constructor(){
    super();
    this.state = {
      title: "", 
      content: "",
      access: "",
      errors: {title: [], content: []}
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

//   static getDerivedStateFromProps(nextProps, prevState){
//    if(nextProps.someValue!==prevState.someValue){
//      return { someState: nextProps.someValue};
//   }
//   else return null;
// }

  componentDidUpdate(prevProps, prevState){
    if(prevProps.editValues !== this.props.editValues){
      this.setState({...this.state, ...this.props.editValues})
    }
  }

  checkForErrors(event){
    const { name, value } = event.target;
    const rules = this.props.validate[name];
    if(rules){
      const errors = validate(rules, name, value);
      // event.target.style.borderColor = 
      //   errors.length !== 0 ? 
      //   'hsl(0, 90%, 45%)' : 
      //   'hsl(0, 0%, 80%)';
      this.setState(prevState => ({errors: {...prevState.errors, [name]: errors}}));   
    }
  }
 
  handleChange(event){
    this.setState({[event.target.name]: event.target.value}, this.checkForErrors(event));
  }

  handleSubmit(event) {
    event.preventDefault();
    const {errors, ...body} = this.state;
    // const areThereErrors = Object.keys(errors)
    // .map(( number) => errors[number].length)
    // .reduce((total, number) => total + number);

    const areThereErrors = Object.keys(errors)
      .filter(f => errors[f].length !== 0)
      .length ? true : false;

    if(!areThereErrors){
      this.props.handleSubmitArticle(body);
      this.setState({title: "", content: "", errors: {title: [], content: []}});
    }
  }

  handleRadio(daka){
    daka()
  } 

  render(){
    const {submitName} = this.props;
    const {errors} = this.state;
    //console.log(errors);
    console.log(this.state.access)
    return (
      <form>
        <InputField 
          title="Titel"
          name="title" 
          error={errors.title.length > 0 ? true : false}
          errortext={'Titel ' + errors.title.join(' en ')}
          onChange={this.handleChange} 
          value={this.state.title} 
          onBlur={this.handleChange} 
          autoFocus 
        />
        <TextField 
          title= "Content"
          name="content" 
          error={errors.content.length > 0 ? true : false}
          errortext={'Content ' + errors.content.join(' en ')}
          onChange={this.handleChange} 
          value={this.state.content} 
          onBlur={this.handleChange} 
        />

        <Button type="submit" onClick={this.handleSubmit}>{submitName}</Button>
      </form>      
    )
  }
}

export default artcileForm;
import React from 'react';

const styles = {
	error: {
		color:  'hsl(0, 90%, 45%)',
		display: 'inline-block'
	}
}

const InputField = props => {
	const {title = '', error = false, errorText='', type= 'text', id, onChange, value, onBlur, children} = props;
	return <label>{!children ? title : children} 
        <input type={type} id={id} onChange={onChange} value={value} onBlur={onBlur} autoFocus={true}  />
	<span style={styles.error}>{error ? errorText : ''}</span> 
    </label>
}

export default InputField;
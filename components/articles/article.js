import React from 'react';
import './article.scss'
import { Link } from 'react-router-dom';

const Article = ({id, title, intro, content, url, isAuth, children}) => {
//const isLoggedIn = true;
const tags = ['code', 'js', 'php'];
const tag = tags[Math.floor(Math.random() * tags.length)];
const fabs = tag !== 'code' ? 'fab' : 'fas';
  return (
  <article className='article'>
    <header className='article-header'>
      
      <h1>{title}</h1>
       <div className="article-showdate"><span>Geschreven op Woensdag 10 april 2019</span></div>
    </header>
   
    <div className='article-content'>
      <div className="article-image"><i className={`article-${tag} ${fabs} fa-${tag}`}></i></div>
      <div className='article-block'> 
        <p>{
          content.split('\n').map((item, key) => (
          <span key={key}>{
            item.split('**').map((item, key) => (
              <span key={key}>{item}</span>)
            )

          }<br/></span>
        ))
      }</p>
      </div>
    </div>
    <footer className='article-footer'>
      {isAuth ? 
        <Link className="article-button article-button-orange" to={{pathname: `${url}/aanpassen`, state: { id: id}}}> 
          Aanpassen
        </Link> : 
      ''}
      {children}
    </footer>
  </article>
  )
}

export default Article;

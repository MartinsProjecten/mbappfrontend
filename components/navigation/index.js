import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import logout from './../auth/logout';
import './navigation.scss';
//import logo from './../../logo.png';

const Navigation = props => {

	return (
		<nav className='nav'>
   
			<NavLink to="/artikelen" className='sel' activeClassName={"selected"}>
        <span>ARTIKELEN</span>
      </NavLink>
			<NavLink to="/projecten" className='sel' activeClassName="selected">
        <span>PROJECTEN</span>
      </NavLink>
			<NavLink to="/info" className='sel' activeClassName="selected">
        <span>INFORMATIE</span
      ></NavLink>
      {props.isAuth ? <NavLink to="/" onClick={() => logout(props.setIsAuth2)}>
        <span> LOGOUT</span
      ></NavLink> : ''}

    </nav>  

	);
}
const mapStateToProps = state => ({
  isAuth: state.authReducer.isAuth
});

const mapDispatchToProps = dispatch => ({
  setIsAuth2: () => dispatch({type: 'IS_AUTHENTICATED2'}),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navigation));
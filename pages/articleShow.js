import React from 'react';
import Article from './../components/articles/article';
import Loader from './../components/helpers/loader/index'
import { connect } from 'react-redux';

class articleShow extends React.Component {

	constructor(props){
    super(props);
    this.state = {
      article: {}
    }
  }

  componentDidMount(){
    const requestArticle = async () => {
      //const {id} = this.props.match.params;
      const { id } = this.props.location.state;
      const { title } = this.props.match.params;
      const articleFind = article => id ?  article.id === id : article.title === title.replace(/-/g, ' ')  
      const article = this.props.articles.find(articleFind);

      if(article){ 
        return this.setState({article: article})
      }
    
      const response = await fetch(`http://localhost/mbapp/public_html/api/articles/${id}`);
      if (response.status === 200){
        const json = await response.json();
        return this.setState({article: json});
      }
      return this.props.history.push('/artikelen');
      }
     requestArticle();
  }


  render(){
    const {article} = this.state;
    if(!article.id){
      return Loader();
    }
    return (<Article key={article.id} {...article} />);
  }  

}

const mapStateToProps = state => ({
  articles: state.articleReducer.articles,
});

export default connect(mapStateToProps)(articleShow);
import React from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Articles from './../components/articles/index';
import ArticleCreate from './../components/articles/articleCreate';
import ArticleEdit from './../components/articles/articleEdit';
import Projects from './../components/projects';
import AllProjects from './../components/projects/AllProjects';
import HocTest from './../components/projects/hoctest';
import Information from './../components/information/index';
import ArticleShow from './articleShow';
import Login from './../components/auth/login';
import Authenticator  from './../components/auth/authenticator';

import CreateProject from './../components/projects/CreateProject';
import EditProject from './../components/projects/EditProject';

// const PrivateRoute = ({ component: Component, isAuth=true,  ...rest }) => (
//   <Route {...rest} render={(props) => {
//     console.log(isAuth);
//     return isAuth === true
//       ? <Component {...props} />
//       : <Redirect to='/artikelen' />
//   }} />
//   )


const PrivateRoute = ({ component: Component, ...rest }) => {
  const auth = Authenticator(rest.isAuth, rest.setIsAuth)
  return <Route {...rest} render={props => (
    auth === true
    ? <Component {...props} />
    : <Redirect to='/' />
  )} />
}

const P = ({ components: Components, ...rest }) => {
  const auth = Authenticator(rest.isAuth, rest.setIsAuth);
  return <Route {...rest} render={props => {
    if(auth) {
      return <Components.allProjects {...props} /> 
    }
    else { 
      return <Components.projects {...props} /> 
    }
  }} />
}

//components={{main: Groups, sidebar: GroupsSidebar}}

const PrivatePublicRoute = ({ component: Component, ...rest }) => {
  Authenticator(rest.isAuth, rest.setIsAuth)
  return <Route {...rest} render={props => (
   <Component {...props} />
  )} />
}


const Pages = ({isAuth, setIsAuth}) => {
	return <Switch>
    <Route exact path="/" component={Articles} />
    <Route exact path="/artikelen" component={Articles} />
    <PrivateRoute exact path="/artikelen/toevoegen" isAuth={isAuth} setIsAuth={setIsAuth} component={ArticleCreate} />
    <PrivateRoute exact path="/artikelen/:title/aanpassen" isAuth={isAuth} setIsAuth={setIsAuth} component={ArticleEdit} />
    <Route exact path="/artikelen/:title" component={ArticleShow} />
    
    <P exact path="/projecten" components={{allProjects: AllProjects, projects: Projects}} isAuth={isAuth} setIsAuth={setIsAuth} />
    // <Route exact path="/projecten/test" component={HocTest} />
   
    <PrivateRoute exact path="/projecten/toevoegen" isAuth={isAuth} setIsAuth={setIsAuth} component={CreateProject} />
    <PrivateRoute exact path="/projecten/:uid/aanpassen" isAuth={isAuth} setIsAuth={setIsAuth} component={EditProject} />
    <Route exact path="/info" component={Information} />
    <Route exact path="/login" component={Login} />
    
  </Switch>}

const mapStateToProps = state => ({
  isAuth: state.authReducer.isAuth,
});

const mapDispatchToProps = dispatch => ({
  setIsAuth: () => dispatch({type: 'IS_AUTHENTICATED'})
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Pages));
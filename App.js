import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store/index';
import Navigation from './components/navigation';
import Pages from './pages';
import mblogo from './g4284.png';
import './App.scss';

class App extends Component {
  render() {
    return (
      <BrowserRouter basename='/mbapp/public_html'>
        <Provider store={store}>
         <div className="App">
            <header>
              <img className='image' src={mblogo} alt="MB-Logo" />
             	<Navigation />
            </header>
            <main>
              <section>
      		      <Pages />
              </section>
           </main>
          </div>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;

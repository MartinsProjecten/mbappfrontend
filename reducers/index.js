import { combineReducers } from 'redux';
import articleReducer from './articleReducer';
import projectReducer from './projectReducer';
import authReducer from './authReducer';

const rootReducer = combineReducers({
	articleReducer,
	authReducer,
	projectReducer
});

export default rootReducer;
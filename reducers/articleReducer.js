const initialState = {
    //articles: [ {title: "testedit", intro: "testing twice", content: "wat nou", source: "https://martinbronsema.nl"}],
    articles: [],
    articlesFetchedFromApi : false
}

const articleReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'ADD_ARTICLE':
			return {...state, articles: [action.payload, ...state.articles]};
		case 'UPDATE_ARTICLE':
		console.log('action', action)
			const articless = state.articles.map(article => 
				article.id === action.id ? 
				{...article, ...action.payload}
				: article );
			return {...state, articles: [...articless]};
		case 'DELETE_ARTICLE':
			const filterArticles = state.articles.filter(article => article.id !== action.id);
			return {...state, articles: [...filterArticles]};
		case 'ARTICLES_FETCHED_FROM_API': 
			return {...state, articlesFetchedFromApi: action.payload}
		default:
			return state;
	}
}

export default articleReducer;
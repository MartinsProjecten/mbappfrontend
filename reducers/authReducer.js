const initialState = {
    token: null,
    isAuth: false,
    isloggedIn: false
}

const authReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'SET_TOKEN':
			return {...state, token: action.payload}
		case 'REMOVE_TOKEN':
			return {...state, token: null, isAuth: false}
		case 'IS_AUTHENTICATED': {
			return {...state, isAuth: true}
		}
		case 'IS_AUTHENTICATED2': {
			return {...state, isAuth: false}
		}
		case 'IS_LOGGED_IN': {
			return {...state, isloggedIn: action.payload}
		}
		default:
			return state;
	}
}

export default authReducer;
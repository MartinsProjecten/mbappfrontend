const initialState = {
    projects: [],
    allProjects: [],
    isAuth: false
}

const projectsReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'ADD_ALL_PROJECTS':
			return {...state, allProjects: action.payload};
		case 'ADD_PROJECTS':
			return {...state, projects: action.payload};
		case 'ADD_PROJECT':
			return {...state, projects: [action.payload, ...state.projects]};
		case 'UPDATE_PROJECT':
			const projectUpdate = state.projects.map(project => 
				project.uid === action.uid ? 
				{...project, ...action.payload}
				: project );
			return {...state, projects: [...projectUpdate]};
		case 'DELETE_PROJECT':
			const filterprojectss = state.projectss.filter(projects => projects.id !== action.id);
			return {...state, projects: [...filterprojectss]};
		case 'CLEAR_PROJECT':
			return {...state, projects: [], isAuth: !state.isAuth};
		default:
			return state;
	}
}

export default projectsReducer;
